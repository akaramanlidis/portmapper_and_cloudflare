#!/bin/bash

if [[ $EUID -ne 0 ]]; then
	echo "This script must be run as root"
	exit 74
fi

function setup() {
	if [ ! -f "/etc/cloudflare_ip.conf" ]; then
		if [ ! -f "`which jq`" ] || [ ! -d "`which dig`" ]; then
			echo "I'll install jq or/and dig for you. Be thankful."
			apt-get install -qq jq dnsutils
			rc=$?
			if [ "$rc" != "0" ]; then
				echo "something went wrong when i tried to install jq."
				echo "Please try in manuelly with `apt-get install jq`"
				exit 0
			fi
		fi
		echo "First of all we NEED your global API Key, and your mail from cloudflare."
		echo "Without that this script is totally useless"
		echo ""
		echo "Please enter the API Global Key. You can look into README.md to see where to find it. NO ECHO!"
		read -s globalkey
		echo "Please enter your e-mail that is registered with the needed cloudflare account. NO ECHO!"
		read -s emailsetup
		echo "We will now save your API-Key and E-Mail."
		echo "Because of security reasons we won't print it. If any actions fail, please check the Api Key in the config"
		echo "The config is located in /etc/cloudflare_ip.conf"
		echo "apikey=$globalkey" > /etc/cloudflare_ip.conf
		echo "email=$emailsetup" >> /etc/cloudflare_ip.conf

		apikey=$(echo $globalkey)
		email=$(echo $emailsetup)
		echo "Get Zone-ID"
		echo "zoneid=$(curl --silent -X GET "https://api.cloudflare.com/client/v4/zones" \
			     	    -H "X-Auth-Email: $email" \
				    -H "X-Auth-Key: $apikey" \
			     	    -H "Content-Type: application/json" | jq '.result[0].id' | sed 's/"//g')"	>> /etc/cloudflare_ip.conf # should be inproved so that is is able to handle multiple domains
		echo "Write Zone-ID in config"
		zoneid=$(cat /etc/cloudflare_ip.conf | grep zoneid | cut -d '=' -f 2)
		echo "Get Domainname"
		echo "domainname=$(curl --silent -X GET "https://api.cloudflare.com/client/v4/zones/$zoneid" \
		                    -H "X-Auth-Email: $email" \
				    -H "X-Auth-Key: $apikey" \
				    -H "Content-Type: application/json" | jq '.result.name' | sed 's/"//g')" >> /etc/cloudflare_ip.conf
		echo "Domainname is $domainname"
		domainname=$(cat /etc/cloudflare_ip.conf | grep domainname | cut -d '=' -f 2)
		echo "Get Recordsdata"
		recordoutput="$(curl --silent -X GET "https://api.cloudflare.com/client/v4/zones/$zoneid/dns_records" \
			             -H "X-Auth-Email: $email" \
				     -H "X-Auth-Key: $apikey" \
				     -H "Content-Type: application/json" )"
		echo "Got Records Data"
		echo "Get Number of records"
		numberofrecords=$( echo $recordoutput | sed -e 's/"/\n /g' | grep -v zone_name | grep -A 2 name | grep $domainname | wc -l)
		echo "Got Number of records: $numberofrecords"
		if [ "$numberofrecords" != "0" ]; then
			echo "We have found more than one record for your domain. Which one do you want to use:"
			records=$(echo $recordoutput" | sed -e 's/,/\n /g' | grep 'name":)
			echo "$recordoutput" | sed -e 's/,/\n /g' | grep 'name":' # We need types here too........
			read dnsname
			if [ "$dnsname" = " " ] || [ "$dnsname" = "" ] || [ "$( echo $records | grep -i "\<$dnsname\>" | wc -l )" = "0" ]; then
				echo "Answer incorrect. Exiting"
				exit 6
			else
				echo "Okay, we will use it then"
			fi
			dnsid=$(echo "$recordoutput" | sed -e 's/,/\n /g' | grep "$dnsname" -B 2 | grep '"id"' | cut -d ':' -f 2 | cut -d '"' -f 2)
			dns_record_type=$(echo "$recordoutput" | sed -e 's/"/\n /g' | grep -v zone_name | grep -B 6 $dnsname | grep type -A 2 | tail -n 1 | sed 's/ //g')
			echo "dnsid=$dnsid" >> /etc/cloudflare_ip.conf
			echo "dnstype=$dns_record_type" >> /etc/cloudflare_ip.conf
			echo "dnsname=$dnsname" >> /etc/cloudflare_ip.conf
		else
			echo "We can't work with a zone that has no DNS Entrys. Aborting."
			exit 5
		fi
		echo "Setup should be finished now. Restart Script to run it for it's purpose."
		echo ""
		echo "Please set the crontab entry needed AS ROOT!"
		echo "To do that execute \"crontab -e\" and paste the following line"
		chmod +x ~/portmapper_and_cloudflare/cloudflare_ip_change.sh
		echo '* * * * * /root/portmapper_and_cloudflare/cloudflare_ip_change.sh'
		echo "Then exit the crontab and everything should be start to work by itself"
		echo "Now you should setup the get_ip.sh part described in README"
		echo "No worries, it's super easy."
		echo "Just add the following line to your crontab as well:"
		echo '* * * * * /root/portmapper_and_cloudflare/get_ip.sh'
		exit 5
	fi
}

function getVars() {
		echo "Getting Variables"
	        dnsname=$(cat /etc/cloudflare_ip.conf | grep dnsname | cut -d '=' -f 2)
		domainname=$(cat /etc/cloudflare_ip.conf | grep domainname | cut -d '=' -f 2)
	        apikey=$(cat /etc/cloudflare_ip.conf | grep apikey | cut -d '=' -f 2)
	        email=$(cat /etc/cloudflare_ip.conf | grep email | cut -d '=' -f 2)
	        dnsid=$(cat /etc/cloudflare_ip.conf | grep dnsid | cut -d '=' -f 2)
	        zoneid=$(cat /etc/cloudflare_ip.conf | grep zoneid | cut -d '=' -f 2)
		dnstype=$(cat /etc/cloudflare_ip.conf | grep dnstype | cut -d '=' -f 2)
}

function check_config() {
	echo "Checking config"
	if [ "$(cat /etc/cloudflare_ip.conf | wc -l)" != "7" ] || [ "$(echo $dnsname | wc -c)" -lt "3" ] || [ "$(echo $apikey | wc -c)" -lt "20" ] || [ "$(echo $email | wc -c)" -lt "5" ] || [ "$(echo $dnsid | wc -c)" -lt "20" ] || [ "$(echo $zoneid | wc -c)" -lt "20" ] || [ "$(echo $domainname | wc -c)" -lt "3" ]; then
		echo "config is broken. We'll run a new setup if you PRESS ENTER to fix this issue. If now PRESS CTRL+C"
		read trash
		mv /etc/cloudflare_ip.conf /etc/cloudflare_ip.conf.bak
	        setup
	fi	
}


setup

getVars

check_config
if [ "$(ps aux | grep -i cloudflare_ip_change.sh | grep -v grep | wc -l)" = "0" ]; then
	ipinit=$(dig $dnstype $dnsname | grep -A 1 'ANSWER SECTION' | grep $dnsname | awk '{print $5}' )
	echo "This is IP: $ipinit"
       	echo "$ipinit" >> /tmp/cloudflare_change.txt
        while /bin/true; do
		ip2=$(cat /tmp/cloudflare_change.txt)
		ip=$(ssh -q getip@getipv6.ipv10.de -i /root/.ssh/id_rsa "$ip2" )
		if [ "$ip" != "$ip2" ]; then
			echo "Changeing DNS from $ip to $ip2" >> /tmp/cloudflare.log
			curl -X PUT "https://api.cloudflare.com/client/v4/zones/$zoneid/dns_records/$dnsid" \
			     -H "X-Auth-Email: $email" \
	        	     -H "X-Auth-Key: $apikey" \
	          	     -H "Content-Type: application/json" \
			     --data '{"type":"'$dnstype'","name":"'$dnsname'","content":"'$ip'","ttl":120,"proxied":false}'
			#ns=$(dig NS $dnsname | grep -A 1 'AUTHORITY SECTION' | grep $domainname | awk '{print $5}')
			#ip=$(dig $dnstype $dnsname @$ns | grep -A 1 'ANSWER SECTION' | grep $dnsname | awk '{print $5}' )
			echo "$ip" > /tmp/cloudflare_change.txt
		fi
		sleep 5
	done
fi
