This Script is currently capable of setting up a deamon, that changes the IP of a cloudflare record fastly.

IMPORTANT THINGS FIRST:

Do EVERYTHING described below as ROOT.

It needs two other services to run properly. A webserver that can return the clients IP-Adress 
and a second script that asks the webserver every 10 seconds which IP he has.

The second Skript is called get_external_ip.sh and will be provided here soon.
Webserver will be reachable with the command "curl -s checkip.dyndns.org | sed -e 's/.*Current IP Address: //' -e 's/<.*$//'" 
An alternative is "curl ipecho.net/plain ; echo" which will be used as backup

This command has yet to be tested for IPv6 IP's.


The main Script is stable, tested and works as designed right now.

Cloudflare API Key:

- Login to Cloudflare
- Click on Your profile and select "My Profile"
- Generate the global API Key


Setup:

- Root login onto your mashine
- git clone https://gitlab.com/akaramanlidis/portmapper_and_cloudflare
- bash /root/portmapper_and_cloudflare/cloudflare_ip_change.sh
- Give the script the information it's needed
- Do what the script tells you to do
- Do exactly what it tells you...


Connecting with portmapper is not yet done. But we'll work on that with a clean solution till Noon.
That's because it's like a "special feature" that isn't really purpose of this Script.
